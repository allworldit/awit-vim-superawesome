## Clone the repository into your ~/.vim directory

	cd

	git clone https://gitlab.devlabs.linuxassist.net/allworldit/awit-vim-superawesome.git .vim


## Install packages we need...

	pacman -S powerline-fonts vim-airline vim-fugitive vim-gitgutter vim-neocomplete vim-securemodelines vim-syntastic


## Set your terminal font to...

	Liberation Mono


## Languages

### Python

The best python linter by far is pylama, this is what we have enabled.

You'll need to install pylama if you want the builtin checks done.

