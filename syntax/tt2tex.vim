" Language:      TT2 embedded with Latex
" Maintainer:    Nigel Kukard <nkukard@lbsd.net>
" Author:        Nigel Kukard <nkukard@lbsd.net>
" Homepage:      none
" Bugs/requests: none
" Last Change:   2017-05-06

if exists("b:current_syntax")
    finish
endif

runtime! syntax/plaintex.vim
unlet b:current_syntax

runtime! syntax/tt2.vim
unlet b:current_syntax

syn cluster texPreProc add=@tt2_top_cluster

let b:current_syntax = "tt2tex"
